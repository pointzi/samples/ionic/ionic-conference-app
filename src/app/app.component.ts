import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {Router} from '@angular/router';
import {SwUpdate} from '@angular/service-worker';

import {Events, MenuController, Platform, ToastController} from '@ionic/angular';

import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {StatusBar} from '@ionic-native/status-bar/ngx';

import {Storage} from '@ionic/storage';

import {UserData} from './providers/user-data';

const PROD_APP_KEY = 'PZDynamic_bison';
const DEV_APP_KEY = 'PZDynamic_zebra';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class AppComponent implements OnInit {
    appPages = [
        {
            title: 'Schedule',
            url: '/app/tabs/schedule',
            icon: 'calendar'
        },
        {
            title: 'Speakers',
            url: '/app/tabs/speakers',
            icon: 'contacts'
        },
        {
            title: 'Map',
            url: '/app/tabs/map',
            icon: 'map'
        },
        {
            title: 'About',
            url: '/app/tabs/about',
            icon: 'information-circle'
        }
    ];
    loggedIn = false;

    constructor(
        private events: Events,
        private menu: MenuController,
        private platform: Platform,
        private router: Router,
        private splashScreen: SplashScreen,
        private statusBar: StatusBar,
        private storage: Storage,
        private userData: UserData,
        private swUpdate: SwUpdate,
        private toastCtrl: ToastController,
    ) {
        this.initializeApp();
    }

    async ngOnInit() {
        this.checkLoginStatus();
        this.listenForLoginEvents();

        this.swUpdate.available.subscribe(async res => {
            const toast = await this.toastCtrl.create({
                message: 'Update available!',
                showCloseButton: true,
                position: 'bottom',
                closeButtonText: `Reload`
            });

            await toast.present();

            toast
                .onDidDismiss()
                .then(() => this.swUpdate.activateUpdate())
                .then(() => window.location.reload());
        });
    }

    initializeApp() {
        this.platform.ready().then(() => {
            this.statusBar.styleDefault();
            this.splashScreen.hide();
            this.initPointzi();
        });
    }

    checkLoginStatus() {
        return this.userData.isLoggedIn().then(loggedIn => {
            return this.updateLoggedInStatus(loggedIn);
        });
    }

    updateLoggedInStatus(loggedIn: boolean) {
        setTimeout(() => {
            this.loggedIn = loggedIn;
        }, 300);
    }

    listenForLoginEvents() {
        this.events.subscribe('user:login', () => {
            this.updateLoggedInStatus(true);
        });

        this.events.subscribe('user:signup', () => {
            this.updateLoggedInStatus(true);
        });

        this.events.subscribe('user:logout', () => {
            this.updateLoggedInStatus(false);
        });
    }

    logout() {
        this.userData.logout().then(() => {
            return this.router.navigateByUrl('/app/tabs/schedule');
        });
    }

    openTutorial() {
        this.menu.enable(false);
        this.storage.set('ion_did_tutorial', false);
        this.router.navigateByUrl('/tutorial');
    }

    initPointzi() {
        let websdkBranch = window.localStorage.getItem('websdkBranch'),
            isUseDev = websdkBranch && websdkBranch === '2';

        // Ask for using sdk dev/prod if not set
        if (!websdkBranch) {
            navigator['notification'].confirm(
                'Do you want to use websdk dev/prod branch?',  // message
                (results) => {
                    websdkBranch = results + '';
                    isUseDev = websdkBranch && websdkBranch === '2';
                    window.localStorage.setItem('websdkBranch', websdkBranch);
                    this.loadPointzi(isUseDev);
                },                  // callback to invoke
                'Web sdk setup',            // title
                ['Prod', 'Dev'],             // buttonLabels
            );
        } else {
            this.loadPointzi(isUseDev);
        }
    }

    loadPointzi(isUseDev) {
        if (window['pointzi']) {
            window['pointzi'].load(isUseDev).then(function () {
                let appkey = window.localStorage.getItem('appkey');

                let promise: Promise<any>;

                if (appkey !== '' && appkey !== null && appkey !== undefined) {
                    promise = Promise.resolve(true);
                } else {
                    promise = new Promise((rs, rj) => {
                        navigator['notification'].confirm(
                            'Please choose app key',  // message
                            (results) => {
                                appkey = results + '' === '1' ? PROD_APP_KEY : DEV_APP_KEY;
                                window.localStorage.setItem('appkey', appkey);
                                rs(appkey);
                            },
                            'App key setup',            // title
                            [PROD_APP_KEY, DEV_APP_KEY],             // buttonLabels
                        );
                    });
                }

                promise.then(() => {
                    const cuid = window.localStorage.getItem('CUID');

                    if (cuid !== '' && cuid !== null && cuid !== undefined) {
                        window['pointzi'].register(appkey, cuid);
                    } else {
                        navigator['notification'].prompt(
                            'Please enter CUID',  // message
                            (res) => {
                                if (res.buttonIndex === 1) {
                                    window.localStorage.setItem('CUID', res.input1);
                                    window['pointzi'].register(appkey, res.input1);
                                }
                            },                  // callback to invoke
                            'CUID',            // title
                            ['Ok', 'Exit'],             // buttonLabels
                            'Jane Doe'                 // defaultText
                        );
                    }
                });
            });
        } else {
            alert('Pointzi plugin not loaded!');
        }
    }
}
